
#![allow(non_snake_case)]
// #[no_mangle]
use gdnative::*;
use gdnative::prelude::*;
// use gdnative::core_types::*;

use midir::{MidiInput, MidiInputConnection, Ignore};

// use midir::Ignore::*;

mod fileReader;
use crate::fileReader::MidiFileHandler;

mod midiUtils;
use crate::midiUtils::MidiUtils;

mod inputHandler;
use crate::inputHandler::InputHandler;


mod notesHandler;
use crate::notesHandler::NotesHandler;


mod common;
use rb::*;

use std::sync::mpsc::channel;
use std::sync::mpsc::{Sender, Receiver};
use crossbeam::atomic::AtomicCell;
#[derive(Default)]
#[derive(Clone)]
#[derive(Copy)]
pub struct MidiMSG (u64, (u8,u8,u8));

const MIDI0 : MidiMSG = MidiMSG(0,(0,0,0));

impl MidiMSG {
  fn to_vec(&self) -> Vec<u8> {
    return vec![(self.1).0,(self.1).1,(self.1).2]
  }
  fn from_vec(v: &Vec<u8>) -> Self {
    return MidiMSG(0,
      (*v.get(0).unwrap_or(&0)
      ,*v.get(1).unwrap_or(&0)
      ,*v.get(2).unwrap_or(&0)))
  }
}

#[derive(Clone)]
#[derive(Copy)]
enum ControlType {
  PitchBend(),
  CCChange(u8)
}

impl Default for ControlType {
  fn default() -> ControlType {
    return ControlType::PitchBend();
  } 
}
impl From<u8> for ControlType {
  fn from(orig: u8) -> Self {
      match orig {
          0 => { return Self::PitchBend()}
          x => { return Self::CCChange(x)}
      };
  }
}
impl Into<u8> for ControlType {
  fn into(self) -> u8 {
      match self {
          Self::PitchBend() => { return 0; }
          Self::CCChange(x) => { return x; }
      };
  }
}




#[derive(Default)]
#[derive(Clone)]
#[derive(Copy)]
// #[derive(Send)]
pub struct JoystickHandler{
  flipX : bool,
  flipY : bool,
}



impl JoystickHandler {
  fn new() -> Self {
    return JoystickHandler {
      flipX : true,
      flipY : false
  };
  }
  // fn loadAxes (&mut self, _owner: &Node, xAxis: VariantArray, yAxis: VariantArray) {

  // }

  fn midToAxisX(&self, indic: u8) -> f32{
    match self.flipX {
      true => {
        return (indic as f32 - 64.) /64.  
      }
      false => {
        return (64. - indic as f32) / 64.
      }
    }
  }

  fn midToAxisY(&self, indic: u8) -> f32{
    match self.flipY {
      true => {
        return (indic as f32 - 64.) /64.  
      }
      false => {
        return (64. - indic as f32) / 64.
      }
    }
  }

}


// type SignalSend = Fn<&Node>;
type SendType  = (Producer<MidiMSG>, 
                  JoystickHandler,  //might be useful later
                  Sender<u8>,       //joystick position
                  Sender<u8>,       //joystick position
                  Receiver<()>,     //waitForJoyXCC  
                  Receiver<()>,     //waitForJoyYCC
                  ) ;

#[derive(gdnative::NativeClass)]
#[inherit(Node)]
#[register_with(Self::register_signals)]
#[user_data(gdnative::nativescript::user_data::MutexData<MidiHandler>)]
struct MidiHandler {
  input :       Option<MidiInput>,
  input_cnx:    Option<MidiInputConnection<SendType>>,
  midi_buffer:  Option<Consumer<MidiMSG>>,
  joystick_handler: JoystickHandler,
  cx: Option<Receiver<u8>>,
  cy: Option<Receiver<u8>>,
  ccConfigX: Option<Sender<()>>,
  ccConfigY: Option<Sender<()>>,
  xPos: u8,
  yPos: u8,
  
}


// #[repr(transparent)]

static channelOverride : AtomicCell<Option<u8>> = AtomicCell::new(None);
static JOY_X_CONTROL : AtomicCell<ControlType> = AtomicCell::new(ControlType::PitchBend());
static JOY_Y_CONTROL : AtomicCell<ControlType> = AtomicCell::new(ControlType::CCChange(1));

fn handleMidiData_new(timestamp: u64, rawmidiMSG: &[u8], sentData: &mut SendType){
  let msg     = match rawmidiMSG.len() {
    0          => {MidiMSG (timestamp, (0,0,0))}
    x if x < 2 => {MidiMSG (timestamp, (rawmidiMSG[0],0,0))}
    x if x < 3 => {MidiMSG (timestamp, (rawmidiMSG[0],rawmidiMSG[1],0))}
    _          => {MidiMSG (timestamp, (rawmidiMSG[0],rawmidiMSG[1],rawmidiMSG[2]))}
  };
  let header  = common::translateToMidiHeader(&msg);
  let channel = channelOverride.load().unwrap_or(header.get(1));

  let ( ref mut prod,ref jsH, ref mut sendX, ref mut sendY, ref mut  axisConfX, ref mut  axisConfY) = *sentData;

  let xAxis = JOY_X_CONTROL.load();
  let yAxis = JOY_Y_CONTROL.load();

  let axisXChange = axisConfX.try_iter().last().is_some();

  let axisYChange = axisConfY.try_iter().last().is_some();

  let passThroughCC = axisXChange || axisYChange ;

  match header.get(0).into() /*midi kind info*/ {
    common::MidiKind::PitchBend => {
      if passThroughCC { prod.write(&[msg]); }
      if axisXChange {
        godot_print!("axis x change");
        JOY_X_CONTROL.store(ControlType::PitchBend());
      }
      if axisYChange {
        godot_print!("axis y change");
        JOY_Y_CONTROL.store(ControlType::PitchBend());
      }
      match xAxis {
        ControlType::PitchBend() => {
          sendX.send(rawmidiMSG[2]);
          
        }
        _ => {}
      }
      match yAxis {
        ControlType::PitchBend() => {
          sendY.send( rawmidiMSG[2]);
        }
        _ => {}
      }
    }
    common::MidiKind::SetParameter => {
      if passThroughCC { prod.write(&[msg]);}
      if axisXChange {
        JOY_X_CONTROL.store(ControlType::CCChange(rawmidiMSG[1]));
      }
      if axisYChange {
        JOY_Y_CONTROL.store(ControlType::CCChange(rawmidiMSG[1]));
      }
      match xAxis {
        ControlType::CCChange(n) => {
          if n == rawmidiMSG[1] {
            // if it's the right CC number
            sendX.send(rawmidiMSG[2]);
          
          }
          
        }
        _ => {}
      }
      match yAxis {
        ControlType::CCChange(n) => {
          if n == rawmidiMSG[1] {
            // if it's the right CC number
            sendY.send( rawmidiMSG[2]);
          }
        }
        _ => {}
      }
    }
    common::MidiKind::NoteOn => {
      prod.write(&[msg]);
    }
    common::MidiKind::NoteOff => {
      prod.write(&[msg]);
    }
    common::MidiKind::NoteKeyPressure => {
      prod.write(&[msg]);
    }
    common::MidiKind::SetProgram => {
      prod.write(&[msg]);
    }
    _ => {}
  }
}
// __One__ `impl` block can have the `#[methods]` attribute, which will generate
// code to automatically bind any exported methods to Godot.
#[methods]
impl MidiHandler {
    
    /// The "constructor" of the class.
    // fn _init(_owner: Node) -> Self {
    //     MidiHandler{
    //         tileSize: BASE_SIZE
    //     }
    // }
    

    fn new(_owner: &Node) -> Self {
      let mut midi_in = MidiInput::new("Maintenant MIDI IN").ok();
      match &mut midi_in {
        Some(input) => {input.ignore( Ignore::SysexAndTime | Ignore::ActiveSense);}
        _ => {}
      }
      MidiHandler {
        input: midi_in,
        input_cnx: None,
        midi_buffer: None,
        joystick_handler: JoystickHandler::new(),
        xPos: 64,
        yPos: 64,
        cx: None,
        cy: None,
        ccConfigX: None,
        ccConfigY: None
      }
    }
    fn register_signals(builder: &ClassBuilder<Self>) {
      builder.add_signal(Signal {
        name: "portConnected",
        // Argument list used by the editor for GUI and generation of GDScript handlers. It can be omitted if the signal is only used from code.
        args: &[SignalArgument {
            name: "success",
            default: Variant::from_i64(0),
            export_info: ExportInfo::new(VariantType::I64),
            usage: PropertyUsage::DEFAULT,
        }],
      });
      builder.add_signal(Signal {
        name: "portClosed",
        // Argument list used by the editor for GUI and generation of GDScript handlers. It can be omitted if the signal is only used from code.
        args: &[SignalArgument {
            name: "success",
            default: Variant::from_i64(0),
            export_info: ExportInfo::new(VariantType::I64),
            usage: PropertyUsage::DEFAULT,
        }],
      });
      builder.add_signal(Signal {
        name: "midiMessage",
        // Argument list used by the editor for GUI and generation of GDScript handlers. It can be omitted if the signal is only used from code.
        args: &[SignalArgument {  
                name: "timestamp",
                default: Variant::from_u64(0),
                export_info: ExportInfo::new(VariantType::I64),
                usage: PropertyUsage::DEFAULT,
            } ,SignalArgument {
                name: "header",
                default: Variant::from_byte_array(&TypedArray::new()),
                export_info: ExportInfo::new(VariantType::I64),
                usage: PropertyUsage::DEFAULT,
            } ,SignalArgument {
                name: "message",
                default: Variant::from_byte_array(&TypedArray::new()),
                export_info: ExportInfo::new(VariantType::I64),
                usage: PropertyUsage::DEFAULT,
      }],
      });
    }

    #[export]
    unsafe fn selectPort(&mut self, owner: &Node, portIndex: i64) {
      match &self.input {
        Some(_mInput) => {  
          let aMInput = self.input.take().unwrap();
          let ports = aMInput.ports();
          const SIZE: usize = 128;
          let rb = SpscRb::new(SIZE);
          let (prod, cons) = (rb.producer(), rb.consumer());
          let (tx, rx): (Sender<u8>, Receiver<u8>) = channel();
          let (ty, ry): (Sender<u8>, Receiver<u8>) = channel();
          let (ccSx, ccRx): (Sender<()>, Receiver<()>) = channel();
          let (ccSy, ccRy): (Sender<()>, Receiver<()>) = channel();
          
          let connectResult = aMInput.connect(
            &ports[portIndex as usize],
            "Maintenant: Midi IN",
            handleMidiData_new,
            (prod
              ,self.joystick_handler
              ,tx
              ,ty
              ,ccRx
              ,ccRy)
          );
          match connectResult {
            Ok(mInputCnx)  => {
              self.input = None;
              self.input_cnx = Some(mInputCnx);
              self.midi_buffer = Some(cons);
              self.cx = Some(rx);
              self.cy = Some(ry);
              self.ccConfigX = Some(ccSx);
              self.ccConfigY = Some(ccSy);
              // godot_print!("port successully connected");
              owner.emit_signal("portConnected", &[Variant::from_u64(1)]);
              },
            Err(mInputError) => {
              // godot_print!("failed to connect");
              self.input = Some(mInputError.into_inner());
              owner.emit_signal("portConnected", &[Variant::from_u64(0)]);
            } 
          }
        }
        _ => {}
      }
      

    }

    #[export]
    fn closePort(&mut self, owner: &Node) {
      match &self.input_cnx {
        Some(_mInputCnx) => {
          let aMInputCnx = self.input_cnx.take().unwrap();
          let (newInput, _) = aMInputCnx.close();
          self.input = Some(newInput);
          owner.emit_signal("portClosed", &[Variant::from_u64(1)]);
        }
        _ => {
          owner.emit_signal("portClosed", &[Variant::from_u64(0)]);
        }
      }
    }

    
    #[export]
    fn listPorts(&self, _owner: &Node) -> TypedArray<GodotString> {
      match &self.input {
        Some(mInput) => {
          let ports = mInput.ports();
          let gstringvec = ports.into_iter().map(|port|{
            return GodotString::from_str(mInput.port_name(&port).unwrap());
          }).collect();
          return TypedArray::from_vec(gstringvec);
        }
         _ => {return TypedArray::new();}
      }
    }
    #[export]
    fn _process(&mut self, owner: &Node, _delta: f64) {
      // godot_print!("test");
      let mut messages =[MIDI0.clone()];
      match &self.midi_buffer {
       Some(cons) => {
        let count = cons.read(&mut messages);
        match count {
          Ok(actual_count) => {
            // godot_print!("received midi message...");
            for i in 0..actual_count { 
              let midiMSG = messages[i];
              let midiHeader = common::translateToMidiHeader(&midiMSG);
              owner.emit_signal("midiMessage"
                  ,&[Variant::from_u64(midiMSG.0)
                  ,Variant::from_byte_array(&midiHeader)
                  ,Variant::from_byte_array(&TypedArray::from_vec(midiMSG.to_vec()))
                ]);

  
            }
          }
          _ => {}
        }
       }
       _ => {}
      }
      match &self.cx {
        Some(receiver) => {
          match receiver.try_iter().last(){
            Some(x) => {self.xPos = x}
            _       => {}
          }
        }
        _ => {}
      }
      match &self.cy {
        Some(receiver) => {
          match receiver.try_iter().last(){
            Some(y) => {self.yPos = y}
            _       => {}
          }
        }
        _ => {}
      }
    }

    #[export]
    fn getStickX(&mut self, _owner: &Node) -> f32 {
      return self.joystick_handler.midToAxisX(self.xPos);
    }
    
    #[export]
    fn getStickY(&mut self, _owner: &Node) -> f32 {
      // let mut sendY = cellX.lock().unwrap();
      return self.joystick_handler.midToAxisY(self.yPos);
    }

        
    // #[export]
    // fn getStick(&mut self, _owner: &Node) -> (f32,f32) {
    //   // let mut sendY = cellX.lock().unwrap();
    //   // return self.joystick_handler.midToAxis(self.yPos);
    //   return self.joystick_handler.midToAxis(self.xPos, self.yPos);
    // }

    #[export]
    fn setChannel(&mut self, _owner: &Node, channel: u8)  {
      channelOverride.store(Some(channel));
    }
    
    
    #[export]
    fn releaseChannel(&mut self, _owner: &Node)  {
      channelOverride.store(None);
    }

    #[export]
    fn setFlipX(&mut self, _owner: &Node, flip: bool)  {
      self.joystick_handler.flipX = flip;
    }
    
    #[export]
    fn setFlipY(&mut self, _owner: &Node, flip: bool) {
      self.joystick_handler.flipY = flip;
    }

    #[export]
    fn setAxisXCC(&mut self, _owner: &Node, new_cc: u8) -> u8 {
      return JOY_X_CONTROL.swap(ControlType::from(new_cc)).into();
    }

    #[export]
    fn setAxisYCC(&mut self, _owner: &Node, new_cc: u8) -> u8 {
      return JOY_Y_CONTROL.swap(ControlType::from(new_cc)).into();
    }

    #[export]
    fn waitAxisXCC(&mut self, _owner: &Node) {
      match &self.ccConfigX{
        Some(s) => {s.send(());}
        _ => {}
      }
    }

    #[export]
    fn waitAxisYCC(&mut self, _owner: &Node) {
      match &self.ccConfigY{
        Some(s) => {s.send(());}
        _ => {}
      }
    }


}

// Function that registers all exposed classes to Godot
fn init(handle: InitHandle) {
    handle.add_class::<MidiFileHandler>();
    handle.add_class::<MidiHandler>();  
    handle.add_class::<MidiUtils>();
    handle.add_class::<InputHandler>();
    handle.add_class::<NotesHandler>();
}



godot_gdnative_init!();
godot_nativescript_init!(init);
godot_gdnative_terminate!();
