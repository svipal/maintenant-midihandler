
#![allow(non_snake_case)]
// #[no_mangle]
use gdnative::*;
use gdnative::prelude::*;

#[path="common.rs"]
mod common;
use common::*;


use std::collections::VecDeque;
use rayon::prelude::*;


#[derive(Copy, Clone, Debug, PartialEq, Eq)]
enum LineState {
  Traveling,
  Ended
}

const MOVESPEED : f32 = 0.008;

#[derive(gdnative::NativeClass)]
#[inherit(Node)]
#[register_with(Self::register_signals)]
pub struct NotesHandler{
    // beginNotes: VecDeque<Begin>,
    // endNotes: VecDeque<End>,
    offset: u16,
    tempo : f32,
    scaleCount: u8,
    beat:  i64,
    notes : Vec<GudNote>,
    noteLines: Vec<(LineState, Ref<Node, Shared>)>,
    noteCreations: Vec<Ref<Node, Shared>>,
    noteDisplayTemplate: Option<(Ref<Node,Shared>,Ref<Node,Shared>)>
}



#[methods]
impl NotesHandler {
  fn new(_owner: &Node) -> Self {
      NotesHandler {
        offset : 12,
        tempo: 0.,
        scaleCount: 12,
        beat: 0,
        notes : Vec::with_capacity(4096),
        noteLines: Vec::new(),
        noteCreations: Vec::new(),
        noteDisplayTemplate: None,
      } 
  }

  fn register_signals(builder: &ClassBuilder<Self>) {
  }
  #[export]
  unsafe fn loadTemplates(&mut self, owner: &Node, lineT: Ref<Node,Shared>, creaT: Ref<Node,Shared>){
    self.noteDisplayTemplate = Some((lineT, creaT));
    let mut crT = creaT.assume_safe();
    (0..self.scaleCount).for_each(|i| {
      let mut cT = crT.duplicate(1+2+4).unwrap().assume_safe();
      owner.add_child(cT, true);
      cT.call("setScalePos",&[Variant::from_i64(i.into())]);
      self.noteCreations.push(cT.claim());
    });
  }
  #[export]
  fn setTempo(&mut self, owner: &Node, tempo: f32){
    self.tempo = tempo;
  }


  #[export]
  unsafe fn makeDisplays(&mut self, owner: &Node) {
    if self.noteDisplayTemplate.is_none() {godot_print!("you need to load the display templates.");return;}
    let mut line = self.noteDisplayTemplate.unwrap().0.assume_safe();
    self.notes.clone().iter().for_each(|&GudNote(start,end,note,velo)|{
      let mut aline = line.duplicate(1+2+4).unwrap().assume_safe();
      aline.call("show",&[]);
      let creator = self.noteCreations[note as usize];
      aline.set("creator", creator);
      aline.set("start", start);
      let uhhstart = aline.get("start").to_f64();
      godot_print!("{}",uhhstart);
      aline.set("end", end);     
      aline.call("setLineInfo", &[Variant::from_i64(start)
                                 ,Variant::from_i64(end)
                                 ,Variant::from_i64(note)
                                 ,Variant::from_i64(velo)]);
      owner.add_child(aline, true);
      self.noteLines.push((LineState::Traveling,aline.claim()));
    });
  }

  //this function turns noteOn and noteOff messages to actual notes with a beginning and an end
  //way easier to represent as moving blocks! 
  #[export]
  fn loadNotes(&mut self, _owner: &Node, rawbNotes : VariantArray, raweNotes : VariantArray) {
      let mut beginNotes: VecDeque<Begin> = VecDeque::with_capacity(4096);
      let mut endNotes:   VecDeque<End> = VecDeque::with_capacity(4096);
      rawbNotes.iter().for_each(|rawb| {
        beginNotes.push_back(Begin( rawb.to_array().get(0).to_i64(),
                                    rawb.to_array().get(1).to_i64(),
                                    rawb.to_array().get(2).to_i64(),));
});
      raweNotes.iter().for_each(|rawe| {
        endNotes.push_back(End( rawe.to_array().get(0).to_i64(),
                                rawe.to_array().get(1).to_i64(),));
      });
      beginNotes.make_contiguous().sort_by(|Begin(time,_,_), Begin(time2,_,_)| {
        time.cmp(time2)
      });
      endNotes.make_contiguous().sort_by(|End(time,_), End(time2,_)| {
        time.cmp(time2)
      });
      
      loop {
        // we take the first noteOn...
        let maybeNB = beginNotes.pop_front();
        match maybeNB {
          Some(noteB) => {
            //needed to reput the endNotes in the correct order !
            let mut rotations  = 0;
            loop {
              // if there are no end notes we just discard it
              if rotations >= endNotes.len(){
                godot_print!("had to discord begin note {:?}", noteB);
                break;
              }
              let potentialE = endNotes.pop_front();
              //we don't construct a note if there are no more endNotes.
              if potentialE.is_none(){break;}

              let noteE = potentialE.unwrap();
              if noteB.1 == noteE.1 {
                let gnote = GudNote(
                  noteB.0,
                  noteE.0,
                  noteB.1,
                  noteB.2
                );
                godot_print!("we made a note ! {:?}", gnote);
                self.notes.push(gnote);
                //we rewind the endNotes....
                endNotes.rotate_right(rotations);
                break;
              } else {
                // we put it back...
               endNotes.push_front(noteE);
               endNotes.rotate_left(1);
               rotations += 1;
              }
            }
          }
          // if there are no more notes, we stop
          None => {
            break;
          }
        }     
      }
      // scale : 0 , number of notes
      

  }


  #[export]
  unsafe fn _process(&mut self, _owner: &Node,delta:f32) {
    self.noteLines.par_iter().for_each(|(state,line)|{
      let mut line = line.assume_safe();
      let start = line.get("start").to_f64();
      let end = line.get("end").to_f64();
      let virtualPos = line.get("virtualPos").to_f64();
      let virtualPos = virtualPos + (delta*self.tempo*MOVESPEED) as f64;
      line.set("virtualPos",
            (virtualPos as f32 + delta*self.tempo*MOVESPEED)
          );
      match state {
        LineState::Ended => {
          
        }
        _ => {
          
          let noteLength = line.call("getLength", &[]).to_f64(); 
          if noteLength <= 0.1 {
            line.call("lightsOff",&[]);
          } else {
            line.call("lightsOn",&[]);
          }
          line.call("setPos",&[Variant::from_f64(virtualPos)]);
        }
      }
    });
  }

  #[export]
  unsafe fn onBeat(&mut self, _owner: &Node,) {
    godot_print!("{}",self.beat);
    //snap
    self.noteLines.iter().for_each(|(mut state,line)|{
      let mut line = line.assume_safe();
      if !(state == LineState::Ended) {
        line.call("setPosBeat", &[Variant::from_i64(self.beat)]);
      }  
    });

    self.beat +=1;
  }
}