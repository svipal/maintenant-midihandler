

#![allow(non_snake_case)]
// #[no_mangle]
use gdnative::*;
use gdnative::prelude::*;

#[path="common.rs"]
mod common;
use common::*;

#[derive(gdnative::NativeClass)]
#[inherit(Node)]
#[register_with(Self::register_signals)]
pub struct InputHandler{
    // midiConsts : Dictionary,
    fold: u8,
    scaleSpan : u8,
    scaleOffset: u8,
    scaleNotes: TypedArray<u8>,
    soundEngine : Option<Ref<Node, Shared>>,
    midiHandler : Option<Ref<Node, Shared>>,
    channel : u8,
}

#[methods]
impl InputHandler {
  fn new(_owner: &Node) -> Self {
    return InputHandler {
      // midiConsts : Dictionary::new(),
      fold        : 0,
      scaleSpan   : 0,
      scaleOffset : 0,
      scaleNotes  : TypedArray::new(),
      soundEngine : None,
      midiHandler : None,
      channel     : 0
    };
  }

  #[export]
  fn linkWithSoundEngine(&mut self, owner: &Node
                        ,soundEngine: Ref<Node,Shared> 
                        ,midiHandler: Ref<Node,Shared>) {
    self.soundEngine = Some(soundEngine);
    self.midiHandler = Some(midiHandler);
  }
  #[export]
  fn loadScale (&mut self, _owner: &Node, span: u8, notes: TypedArray<u8>, offset: u8) {
    self.scaleSpan = span;
    self.scaleOffset = offset;
    self.scaleNotes = notes;
  }
  #[export]
  fn setFold (&mut self, _owner: &Node, fold: u8) {
    self.fold = fold;
  }
  #[export]
  fn setChannel(&mut self, _owner: &Node, channel: u8)  {
    self.channel = channel;
  }
  

  fn register_signals(builder: &ClassBuilder<Self>) {
    builder.add_signal(Signal {
      name: "midiMessage",
      // Argument list used by the editor for GUI and generation of GDScript handlers. It can be omitted if the signal is only used from code.
      args: &[SignalArgument {  
              name: "timestamp",
              default: Variant::from_u64(0),
              export_info: ExportInfo::new(VariantType::I64),
              usage: PropertyUsage::DEFAULT,
            },SignalArgument {
              name: "header",
              default: Variant::from_byte_array(&TypedArray::new()),
              export_info: ExportInfo::new(VariantType::I64),
              usage: PropertyUsage::DEFAULT,
            },SignalArgument {
              name: "message",
              default: Variant::from_byte_array(&TypedArray::new()),
              export_info: ExportInfo::new(VariantType::I64),
              usage: PropertyUsage::DEFAULT,
        }],
    });
  }

  #[export]
  unsafe fn _input(&mut self, owner: &Node, event_ref: Ref<InputEvent,Shared>){
    let event: &InputEvent= event_ref.assume_safe().as_ref();
    let keyEvent : Option<&InputEventKey> = event.cast();
    let engine = self.soundEngine.map(|engine| {
      return engine.assume_safe();
    });
    //channels are used to decide what instrument we're using
    
    let channel = self.channel;
    match keyEvent {
      Some(keyEv) => {
        if keyEv.is_action_pressed("fold+",false){
          self.fold = (self.fold + 1) % (127/self.scaleSpan); //highest possible fold given the current scale's span;
          return ();
        } else if keyEv.is_action_pressed("fold-",false){
          self.fold = (self.fold - 1) % (127/self.scaleSpan); //highest possible fold given the current scale's span;
          return ();
        }
        for i in 1..=self.scaleSpan{
          if keyEv.is_action_pressed("note_".to_owned()+&i.to_string(), false) {
            let input = Input::godot_singleton();
            let mut velocity = 64;
            if Input::is_action_pressed(&input,"vol_attenuator") {
              velocity -= 40;
            } 
            if Input::is_action_pressed(&input,"vol_amplifier") {
              velocity += 63;
            } 
            let note = common::buildNoteMidi(i-1,
              self.fold,
              self.scaleSpan,
              self.scaleNotes.clone(),
              self.scaleOffset);
            let header =  TypedArray::from_vec(vec![
                            1, //NoteOn
                            channel]);//always channel 1 for self emitted messages
            let status = translateFromMidiHeader(&header);
            owner.emit_signal("midiMessage"
              ,&[Variant::from_u64(0) // we have no actual time for now
                ,Variant::from_byte_array(&header)
                ,Variant::from_byte_array(&TypedArray::from_vec(vec![status,
                  note,
                  velocity
                  ]))
                ]);
            engine.map(|engine|{
              engine.call("playMIDI"
                ,&[Variant::from_byte_array(&header)
                  ,Variant::from_byte_array(&TypedArray::from_vec(vec![status,
                  note,
                  velocity
                  ]))
                ]
              );
            });
          } else if keyEv.is_action_released("note_".to_owned()+&i.to_string()) {
            let note = common::buildNoteMidi(i-1,
                                self.fold,
                                self.scaleSpan,
                                self.scaleNotes.clone(),
                                self.scaleOffset);
            let header =  TypedArray::from_vec(vec![
                            0, //NoteOff
                            channel]); //always channel 1 for self emitted messages
            let status = translateFromMidiHeader(&header);
            owner.emit_signal("midiMessage"
              ,&[Variant::from_u64(0) // no actual time for now
                ,Variant::from_byte_array(&header)
                ,Variant::from_byte_array(&TypedArray::from_vec(vec![status,
                  note,
                  channel //no velocity for noteOff
                ]))
              ]);
            engine.map(|engine|{
              engine.call("playMIDI"
                ,&[Variant::from_byte_array(&header)
                  ,Variant::from_byte_array(&TypedArray::from_vec(vec![status,
                    note,
                    0 //no velocity for noteOff
                  ]))
                ]
              );
            });
            }
          }
        }
      _ => {}
    }
  }
}


