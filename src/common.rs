
use gdnative::prelude::*;

use std::fmt;
use crate::MidiMSG;

pub fn buildNoteMidi( noteIndex: u8, fold: u8, scaleSpan: u8,  scaleNotes: TypedArray<u8>, scaleOffset: u8) -> u8 {
    let baseFoldNote = fold*scaleSpan + scaleOffset;
    return scaleNotes.get(noteIndex as i32) + baseFoldNote;
}

  
#[inline]
pub fn translateToMidiHeader(midiMSG : &MidiMSG) -> TypedArray<u8>  {
  let status = (midiMSG.1).0;
  let channel   = status % 16;
  let base_byte =  status - channel; 
  let enum_index = (base_byte - 128) / 16;
  return TypedArray::from_vec(vec![enum_index,channel+1])
}

  
#[inline]
pub fn translateStatusByteToMidiHeader(status: u8) -> TypedArray<u8>  {
  let channel   = status % 16;
  let base_byte =  status - channel; 
  let enum_index = (base_byte - 128) / 16;
  return TypedArray::from_vec(vec![enum_index,channel+1])
}


#[inline]
pub fn translateFromMidiHeader(header: &TypedArray<u8>) -> u8 {  
    let enum_index : MidiKind = header.get(0).into();
    let base_byte = enum_index.to_byte();
    return base_byte + header.get(1) - 1;
}

#[repr(u8)]
#[derive(Copy, Clone, Debug)]
pub enum MidiKind {
    NoteOff = 0,
    NoteOn = 1,
    NoteKeyPressure = 2,
    SetParameter = 3,
    SetProgram = 4,
    ChangePressure = 5,
    PitchBend = 6,
    MetaEvent = 7
   }

impl From<u8> for MidiKind {
    fn from(orig: u8) -> Self {
        match orig {
            0 => { return Self::NoteOff}
            1 => { return Self::NoteOn}
            2 => { return Self::NoteKeyPressure}
            3 => { return Self::SetParameter}
            4 => { return Self::SetProgram}
            5 => { return Self::ChangePressure}
            6 => { return Self::PitchBend}
            _ => { return Self::MetaEvent}
        };
    }
}


impl Into<u8> for MidiKind {
    fn into(self) -> u8 {
        match self {
            Self::NoteOff => { return 0 }
            Self::NoteOn => { return 1 }
            Self::NoteKeyPressure => { return 2 }
            Self::SetParameter => { return 3 }
            Self::SetProgram => { return 4 }
            Self::ChangePressure => { return 5 }
            Self::PitchBend => { return 6 }
            Self::MetaEvent => { return 7 }
        };
    }
}



impl MidiKind {
    pub fn to_byte(&self) -> u8 {
        let ind : u8 = self.clone().into();
        return 128 + ind*16;
    } 
}


impl fmt::Display for MidiKind {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::NoteOff => { write!(f, "NoteOff") }
            Self::NoteOn => { write!(f, "NoteOn") }
            Self::NoteKeyPressure => { write!(f, "NoteKeyPressure") }
            Self::SetParameter => { write!(f, "SetParameter") }
            Self::SetProgram => { write!(f, "SetProgram") }
            Self::ChangePressure => { write!(f, "ChangePressure") }
            Self::PitchBend => { write!(f, "PitchBend") }
            Self::MetaEvent => { write!(f, "MetaEvent") }
        }
    }
        
}


#[derive(Copy, Clone, Debug)]
pub struct Begin(pub i64,pub i64,pub i64); //time in div, note, velo

#[derive(Copy, Clone, Debug)]
pub struct End(pub i64,pub i64); // time in div, note


#[derive(Copy, Clone, Debug)]
pub struct GudNote(
    pub i64,
    pub i64,
    pub i64,
    pub i64) ;//start, end, note, velo
