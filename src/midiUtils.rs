

#![allow(non_snake_case)]
// #[no_mangle]
use gdnative::*;
use gdnative::prelude::*;

#[path="common.rs"]
mod common;

#[derive(gdnative::NativeClass)]
#[inherit(Node)]
pub struct MidiUtils();


#[methods]
impl MidiUtils {
  fn new(_owner: &Node) -> Self {
    MidiUtils {
    }
  }
  #[export]
  fn makeColor(&self, _owner: &Node, octave:u8) -> Color {
    let ocf = octave as f32;
    return Color::rgb((ocf % 3.) / 10.
                      ,(ocf % 4.).powf(2.) / 2.
                      ,((ocf % 7.) / 8.).powf(3.))
  }
  
  #[export]
  fn buildNoteMidi(&self, _owner: &Node, noteIndex: u8, fold: u8, scaleSpan: u8,  scaleNotes: TypedArray<u8>, scaleOffset: u8) -> u8 {
    return common::buildNoteMidi(noteIndex,fold,scaleSpan,scaleNotes,scaleOffset);
  }

  #[export]
  fn getNoteIndexFromCustomScale(&self, _owner: &Node, noteIndic:u8, scaleSpan: u8,  scaleNotes: TypedArray<u8>, scaleOffset: u8) -> TypedArray<u8> {
    let aimNote = (noteIndic - scaleOffset) % scaleSpan;  
    let noteFold  = (noteIndic - scaleOffset) / scaleSpan;
    let mut theNote = 0;
      for i in 0..scaleSpan {
        if aimNote >= scaleNotes.get(i as i32) {
          theNote = i;
        }
      }
    return TypedArray::from_vec(
        vec![theNote, noteFold]);
  }


  #[export]
    fn getNoteIndex(&self, _owner: &Node, noteIndic:u8, noteCount:u8, offset: u8) -> TypedArray<u8> {
      let noteIndex = (noteIndic - offset) % noteCount;
      let noteOctave =  (noteIndic - offset) / noteCount;
      return TypedArray::from_vec(vec![noteIndex,noteOctave])
    }
}

