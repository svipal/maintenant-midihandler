
#![allow(non_snake_case)]
// #[no_mangle]
use gdnative::*;
use gdnative::prelude::*;

#[path="common.rs"]
mod common;
use common::*;

use std::collections::HashMap;
use rimd::*;
use rimd::Event::*;

use std::path::Path;


#[derive(gdnative::NativeClass)]
#[inherit(Node)]
#[register_with(Self::register_signals)]
pub struct MidiFileHandler{
    // midiConsts : Dictionary,
   smfs: HashMap<String,SMF>,
}

#[methods]
impl MidiFileHandler {
  fn new(_owner: &Node) -> Self {
      MidiFileHandler {
        smfs : HashMap::new()
      } 
  }

  fn register_signals(builder: &ClassBuilder<Self>) {
  }

  #[export] 
  fn loadFile(&mut self, owner: &Node, name: String) -> bool {
    let maybesmf = SMF::from_file(
        Path::new(&("./assets/midi/".to_owned()+&name+".mid")));
    maybesmf.map_or_else(|_| {
        return false;
                        },|smf|{
        self.smfs.insert(name,smf);
        return true;
    })
  }
  #[export] 
  fn getDiv(&mut self, _owner: &Node, name: String) -> i16 {
    let smf = self.smfs.get(&name);
    let mut div = 0;
    if smf.is_none() { godot_print!("no such smf ");}
    smf.map(|smf|{
        div=smf.division;
    });
    return div;
  }

  #[export] 
  fn getNotes(&mut self, _owner: &Node, name: String, track: i64) -> Vec<VariantArray<Shared>> {
    let smf = self.smfs.get(&name);
    let mut beginNotes = Vec::<Begin>::new();
    let mut endNotes = Vec::<End>::new();
    let mut div = 0;
    if smf.is_none() { godot_print!("no such smf ");}
    let mut actualTime :u64 = 0;
    smf.map(|smf|{
        div=smf.division; 
        godot_print!("{} tracks", &smf.tracks.len());
        let track = &smf.tracks[track as usize];
        godot_print!("{} events", track.events.len());
        track.events.iter().for_each(|trackevent|{
          let event = &trackevent.event;
          let deltaTime = trackevent.vtime;
          match event {
              Midi(message) => {
                let header = common::translateStatusByteToMidiHeader(message.data[0]);
                let midiheader : MidiKind= header.get(0).into();
                godot_print!("{}",midiheader);
                match header.get(0).into() {
                  MidiKind::NoteOn => {
                    actualTime += deltaTime;
                    let note = message.data.get(1).unwrap();
                    let velo = message.data.get(2).unwrap();
                    godot_print!("{},{},{}",actualTime,*note,*velo);
                    beginNotes.push(Begin  (actualTime as i64
                                          ,*note as i64
                                          ,*velo as i64));
                    

                  }
                  MidiKind::NoteOff => {
                    actualTime += deltaTime;
                    let note = message.data.get(1).unwrap();
                    godot_print!("{},{}",actualTime,*note);
                    endNotes.push(End(actualTime as i64
                                    ,*note as i64));
                    
                  }
                  // we don't care about the others
                  _ => {}
                }
                
                
              }
              // we don't care about those
              Meta(_) => {godot_print!("thats meta");}
          }
        });
    });

    let rbNotes : VariantArray<Unique> = beginNotes.iter().map(|Begin(time,note,velo)|{
      vec![ *time,
            *note,
            *velo]
    }).collect();
    let reNotes : VariantArray<Unique> = endNotes.iter().map(|End(time,note)|{
      vec![ *time,
            *note]
    }).collect();
    let srb = rbNotes.into_shared();
    let sre = reNotes.into_shared();
    return vec! [srb
                ,sre];
  }
}